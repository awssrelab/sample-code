FROM tomcat:latest  

ENV CATALINA_HOME=/usr/local/tomcat/

USER root

COPY target/*.war ${CATALINA_HOME}/webapps/


CMD ["/bin/bash", "-c", "${CATALINA_HOME}/bin/catalina.sh run"] 

